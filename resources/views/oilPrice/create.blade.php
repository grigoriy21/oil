<script>
    function loadDoc(word) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
//                    document.getElementById("demo").innerHTML =
//                        this.responseText;
                $('#nowTranslate').val(this.responseXML.getElementsByTagName('text')[0].childNodes[0].nodeValue);
            }
        };

        xhttp.open("GET", "https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20171014T163154Z.95963d05e566f21d.d142cde7089caf5fd22428237024fd54924c955a&text=" + word + "&lang=en-ru", true);
        xhttp.send();
    }

</script>


<div class="col-sm-8 blog-main">
    <h1>Publish a Post</h1>

    <form method="post" action="/posts">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="word">Enter the english:</label>
            <textarea class="form-control" name="word" onkeyup='loadDoc(this.value)'></textarea>
        </div>
        <div class="form-group">
            <label for="translation">Translation</label>
            <textarea class="form-control" id="nowTranslate" name="translation"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Publish</button>

        @include('layouts.errors')

    </form>


</div>

<br/><br/>
<div class="container" style="width:1100px;">
    <br/><br/>
    <div class="table-responsive">
        <div align="center">
            <?php
            $character = range('A', 'Z');
            echo '<ul class="pagination">';
            foreach ($character as $alphabet) {
                echo '<li><a href="/?char=' . $alphabet . '">' . $alphabet . '</a></li>';
            }
            echo '</ul>';
            ?>
        </div>
        <table class="table table-bordered">
            <tr>
                <th width="50%">Word</th>
                <th width="50%">Translation</th>
            </tr>
            @if($result)
                @foreach ($result as $row)

<!--                    --><?php //dd($row); ?>
                    <tr>
                        <td>{{ $row->word }}</td>
                        <td>{{ $row->translation }}</td>
                    </tr>
                @endforeach

            @else


                <tr>
                    <td colspan="3" align="center">Data not Found</td>
                </tr>
            @endif
        </table>
    </div>
</div>
