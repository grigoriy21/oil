-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.16 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table Oil.OilPrice
CREATE TABLE IF NOT EXISTS `OilPrice` (
  `Volume` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Petrol_98` decimal(3,1) DEFAULT NULL,
  `JET` decimal(4,2) DEFAULT NULL,
  `Petrol_95` decimal(4,2) DEFAULT NULL,
  `Petrol_92` decimal(3,1) DEFAULT NULL,
  `Diesel` decimal(4,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table Oil.OilPrice: ~2 rows (approximately)
/*!40000 ALTER TABLE `OilPrice` DISABLE KEYS */;
INSERT INTO `OilPrice` (`Volume`, `Petrol_98`, `JET`, `Petrol_95`, `Petrol_92`, `Diesel`) VALUES
	('до999л.', 45.8, 39.75, 39.15, 36.7, 39.75),
	('От1000л.', 45.7, 39.45, 38.85, 36.4, 39.35);
/*!40000 ALTER TABLE `OilPrice` ENABLE KEYS */;

-- Dumping structure for table Oil.oilTest_Sheet1
CREATE TABLE IF NOT EXISTS `oilTest_Sheet1` (
  `состояние` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Баланс` decimal(8,1) DEFAULT NULL,
  `дата_отгр` date DEFAULT NULL,
  `Клиент` varchar(29) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `тел` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `плательщик` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `топл` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `к-во` smallint(6) DEFAULT NULL,
  `цена_продажи` decimal(4,2) DEFAULT NULL,
  `сумма_к_оплате` decimal(7,1) DEFAULT NULL,
  `дата_оплаты` date DEFAULT NULL,
  `Оплачено` mediumint(9) DEFAULT NULL,
  `форма_оплаты` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Цена_закупа_безнал` decimal(3,1) DEFAULT NULL,
  `примечание` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table Oil.oilTest_Sheet1: ~39 rows (approximately)
/*!40000 ALTER TABLE `oilTest_Sheet1` DISABLE KEYS */;
INSERT INTO `oilTest_Sheet1` (`состояние`, `Баланс`, `дата_отгр`, `Клиент`, `тел`, `плательщик`, `топл`, `к-во`, `цена_продажи`, `сумма_к_оплате`, `дата_оплаты`, `Оплачено`, `форма_оплаты`, `Цена_закупа_безнал`, `примечание`) VALUES
	('отгружено', -100500.0, '2017-11-15', 'Антон Автогиря от Вадима', '+79043328923', 'Антон Сергеевич С.', 'ДТ', 5000, 33.50, 167500.0, '2017-11-15', 67000, 'сбер', 37.1, ''),
	('', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
	('отгружено', 160.0, '2017-11-15', 'Спец Топливо Эстония', '+79643810583', 'Михаил Викторович С.', 'ДТ', 13700, 33.20, 454840.0, '2017-11-15', 455000, 'нал', 37.1, ''),
	('отгружено', 0.0, '2017-11-15', 'Александр Васильевич К. интер', '+79052637801', 'Александр Васильевич К.', 'ДТ', 400, 34.00, 13600.0, '2017-11-15', 13600, 'нал', 37.1, ''),
	('', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
	('', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
	('отгружено', 0.0, '2017-11-16', 'Владимир', '+79152324242', 'Владимир Валентинович Б.', 'ДТ', 3000, 34.05, 102150.0, '2017-11-17', 102150, 'сбер', NULL, ''),
	('отгружено', 0.0, '2017-11-16', 'ДМИТРИЙ', '+79111697642', 'ДМИТРИЙ АЛЕКСЕЕВИЧ С.', 'ДТ', 1000, 33.95, 33950.0, '2017-11-16', 33950, 'сбер', NULL, ''),
	('отгружено', -110.0, '2017-11-16', 'Дмитрий Юрьевич Н.', '', 'Дмитрий Юрьевич Н.', 'Аи95', 200, 35.60, 7120.0, '2017-11-16', 7010, 'сбер', NULL, ''),
	('отгружено', 10.0, '2017-11-17', 'Кирилл', '+79658188418', 'Кирилл Юрич Б.', 'АИ95', 150, 35.60, 5340.0, '2017-11-20', 5350, 'нал', NULL, ''),
	('отгружено', 0.0, '2017-11-17', 'кл ', '???', '???', 'ДТ', 600, 34.00, 20400.0, '2017-11-17', 20400, 'нал', NULL, ''),
	('отгружено', 0.0, '2017-11-17', 'Бензин АИ95 карта 008856929', '', 'ЮЛИЯ МИХАЙЛОВНА Ш.', 'АИ95', 200, 35.00, 7000.0, '2017-11-13', 7000, 'сбер', NULL, ''),
	('отгружено', -4.0, '2017-11-17', 'АНДРЕЙ АНДРЕЕВИЧ Б.', '', 'АНДРЕЙ АНДРЕЕВИЧ Б.', 'ДТ', 120, 33.95, 4074.0, '2017-11-13', 4070, 'сбер', NULL, ''),
	('', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
	('отгружено', -32750.0, '2017-11-17', 'Влад печнуха', '+79219466735', '', 'АИ92', 1000, 32.75, 32750.0, NULL, NULL, '', NULL, ''),
	('отгружено', -33300.0, '2017-11-17', 'Влад печнуха', '+79219466735', '', 'ДТ', 1000, 33.30, 33300.0, NULL, NULL, '', NULL, ''),
	('отгружено', -44344.0, '2017-11-17', 'Спец Топливо Эстония', '+79643810583', '', 'ДТ', 19680, 33.30, 655344.0, '2017-11-18', 611000, 'нал', NULL, ''),
	('отгружено', 0.0, '2017-11-21', 'АЛЕКСАНДР СЕРГЕЕВИЧ Матвеев', '+79657999799', 'Александр Сергеевич М.', 'ДТ', 300, 33.75, 10125.0, '2017-11-14', 10125, 'сбер', NULL, ''),
	('', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
	('отгружено', 0.0, '2017-11-21', 'Дмитрий Юрьевич Н.', '', 'Дмитрий Юрьевич Н.', 'Аи95', 200, 35.60, 7120.0, '2017-11-21', 7120, 'сбер', NULL, ''),
	('отгружено', 0.0, '2017-11-21', 'Роман ветеранов X6 браток', '+79217601908', 'ЕЛЕНА ЕВГЕНЬЕВНА М.', 'ДТ', 600, 33.95, 20370.0, '2017-11-14', 20370, 'сбер', NULL, ''),
	('отгружено', 0.0, '2017-11-22', 'форд мондео адрей', '+79095905118', 'иван анатольевич б.', 'Аи95', 100, 35.60, 3560.0, '2017-11-22', 3560, 'сбер', NULL, ''),
	('отгружено', 0.0, '2017-11-22', 'евгений', '+79675511111', 'евгений сергеевич з.', 'ДТ', 160, 34.05, 5448.0, '2017-11-22', 5448, 'сбер', NULL, ''),
	('отгружено', -3325.0, '2017-11-24', 'кл из крондштадт', '+79811277815', '', 'АИ92', 100, 33.25, 3325.0, NULL, NULL, 'втб24', NULL, ''),
	('отгружено', -14360.0, '2017-11-22', 'кл', '', '', 'АИ95', 400, 35.90, 14360.0, NULL, NULL, 'сбер', NULL, ''),
	('отгружено', -13300.0, '2017-11-22', 'кл', '', '', 'АИ92', 400, 33.25, 13300.0, NULL, NULL, 'сбер', NULL, ''),
	('', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
	('отгружено', -102150.0, '2017-11-24', 'Владимир', '+79152324242', 'Владимир Валентинович Б.', 'Дт', 3000, 34.05, 102150.0, NULL, NULL, 'сбер', NULL, ''),
	('', -4987.5, '2017-11-24', 'кл беденко м', '', '', 'АИ92', 150, 33.25, 4987.5, NULL, NULL, 'нал', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, ''),
	('', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, ''),
	('', 0.0, NULL, '', '', '', '', NULL, NULL, 0.0, NULL, NULL, '', NULL, '');
/*!40000 ALTER TABLE `oilTest_Sheet1` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
