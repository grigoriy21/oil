<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OilPrice extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'Volume', 'Petrol_98', 'JET','Petrol_95', 'Petrol_92', 'Diesel',
    ];
    protected $table = 'OilPrice';
}
