<?php

namespace App\Http\Controllers;

use App\OilPrice;
use Illuminate\Http\Request;

class OilPriceController extends Controller
{
    public function index()
    {


        $result = OilPrice::all();
        $resultJSON = json_encode($result);

//        dd($resultJSON);
        return view('oilPrice.index', compact( 'result', 'resultJSON'));

    }
}
